/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pokeritilasto2.domain;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Objects;

/**
 *
 * @author Dell
 */
public class Pelipaiva {
    private LocalDate pvm;
    private double tunnit;
    private int turnaustenLkm;
    private double sisaanostot;
    private double voitot;

    public Pelipaiva(LocalDate pvm, double tunnit, int turnaustenLkm, double sisaanostot, double voitot) {
        this.pvm = pvm;
        this.tunnit = tunnit;
        this.turnaustenLkm = turnaustenLkm;
        this.sisaanostot = sisaanostot;
        this.voitot = voitot;
    }

    public Pelipaiva(int tunnit, int turnaustenLkm, double sisaanostot, double voitot) {
        this(LocalDate.now(), tunnit, turnaustenLkm, sisaanostot, voitot);
    }

    public LocalDate getPvm() {
        return pvm;
    }


    public double getTunnit() {
        return tunnit;
    }

    public int getTurnaustenLkm() {
        return turnaustenLkm;
    }

    public double getSisaanostot() {
        return sisaanostot;
    }

    public double getVoitot() {
        return voitot;
    }
    
    public double getTulos() {
        return Math.round((this.voitot - this.sisaanostot) * 100.0) /100.0;
    }
    
    public double getKeskimaarainenSisaanosto()  {
        return Math.round((this.sisaanostot / (double)this.turnaustenLkm) * 100.0 )/ 100.0;
    }
    
    public double getPalkka() {
        return getTulos() / tunnit;
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pelipaiva other = (Pelipaiva) obj;
        if (this.tunnit != other.tunnit) {
            return false;
        }
        if (this.turnaustenLkm != other.turnaustenLkm) {
            return false;
        }
        if (Double.doubleToLongBits(this.sisaanostot) != Double.doubleToLongBits(other.sisaanostot)) {
            return false;
        }
        if (Double.doubleToLongBits(this.voitot) != Double.doubleToLongBits(other.voitot)) {
            return false;
        }
        if (!Objects.equals(this.pvm, other.pvm)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        DateTimeFormatter muotoilija = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        return this.pvm.format(muotoilija) + "," +
                this.tunnit + "," +
                this.turnaustenLkm + "," +
                this.sisaanostot + "," +
                this.voitot;
    }
    
    

    
    
    
    
    
    
    
}
