/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pokeritilasto2.logiikka;

import java.io.File;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import pokeritilasto2.domain.Pelipaiva;
import javafx.util.StringConverter;

/**
 *
 * @author Dell
 */
public class Pelitilasto {

    private List<Pelipaiva> pelitilasto;
    private TableView<Pelipaiva> pelitaulukko;
    private Label roiLabel;
    private Label palkkaLabel;

    public Pelitilasto() {
        this.pelitilasto = new ArrayList<>();
        this.pelitaulukko = new TableView<Pelipaiva>();

        TableColumn pvmSarake = new TableColumn("Päivämäärä");
        pvmSarake.setCellValueFactory(new PropertyValueFactory<Pelipaiva, String>("pvm"));
        pvmSarake.setMinWidth(120);
        TableColumn tunnitSarake = new TableColumn("Tunnit");
        tunnitSarake.setCellValueFactory(new PropertyValueFactory<Pelipaiva, Double>("tunnit"));
        tunnitSarake.setMinWidth(120);
        TableColumn turnaustenLkmSarake = new TableColumn("Turnaukset");
        turnaustenLkmSarake.setCellValueFactory(new PropertyValueFactory<Pelipaiva, Integer>("turnaustenLkm"));
        turnaustenLkmSarake.setMinWidth(120);
        TableColumn sisaanostotSarake = new TableColumn("Sisäänostot");
        sisaanostotSarake.setCellValueFactory(new PropertyValueFactory<Pelipaiva, Double>("sisaanostot"));
        sisaanostotSarake.setMinWidth(120);
        TableColumn voitotSarake = new TableColumn("Voitot");
        voitotSarake.setCellValueFactory(new PropertyValueFactory<Pelipaiva, Double>("voitot"));
        voitotSarake.setMinWidth(120);
        TableColumn tulosSarake = new TableColumn("Tulos");
        tulosSarake.setCellValueFactory(new PropertyValueFactory<Pelipaiva, Double>("tulos"));
        tulosSarake.setMinWidth(120);
        TableColumn keskimaarainenSisaanostoSarake = new TableColumn("Keskim. sisäänosto");
        keskimaarainenSisaanostoSarake.setCellValueFactory(new PropertyValueFactory<Pelipaiva, Double>("keskimaarainenSisaanosto"));
        keskimaarainenSisaanostoSarake.setMinWidth(120);

        this.pelitaulukko.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        this.pelitaulukko.getColumns().addAll(pvmSarake, tunnitSarake, turnaustenLkmSarake, sisaanostotSarake, voitotSarake, tulosSarake,
                keskimaarainenSisaanostoSarake);

        this.palkkaLabel = new Label("");
        this.roiLabel = new Label("");

    }

    public TableView getPelitaulukko() {
        return pelitaulukko;
    }

    public List<Pelipaiva> getPelitilasto() {
        return pelitilasto;
    }

    public void lisaa(Pelipaiva pelipaiva) {
        this.pelitilasto.add(pelipaiva);
        this.pelitaulukko.getItems().add(pelipaiva);
        paivitaRoiJaPalkka();
    }

    public void poista(Pelipaiva poistettava) {
        this.pelitaulukko.getItems().remove(poistettava);
        this.pelitilasto.remove(poistettava);
        paivitaRoiJaPalkka();
    }

    public void tulostaKaikki() {
        for (Pelipaiva pelipaiva : this.pelitilasto) {
            System.out.println(pelipaiva);
        }
    }

    public double getRoi() {
        double tulokset = 0;
        double sisaanosto = 0;
        for (Pelipaiva pelipaiva : this.pelitilasto) {
            tulokset += pelipaiva.getTulos();
            sisaanosto += pelipaiva.getSisaanostot();
        }
        if (sisaanosto == 0) {
            return 0;
        }
        return Math.round((tulokset / sisaanosto) * 10000.0) / 100.0;
    }

    public Label getRoiLabel() {
        return roiLabel;
    }

    public Label getPalkkaLabel() {
        return palkkaLabel;
    }

    public double getPalkka() {
        double tunnit = 0;
        double tulokset = 0;
        for (Pelipaiva pelipaiva : this.pelitilasto) {
            tulokset += pelipaiva.getTulos();
            tunnit += pelipaiva.getTunnit();
        }
        if (tunnit == 0) {
            return 0;
        }
        return Math.round((tulokset / tunnit) * 100.0) / 100.0;
    }

    public void paivitaRoiJaPalkka() {
        Double palkka = getPalkka();
        Double roi = getRoi();

        roiLabel.setText(roi.toString() + "%");
        palkkaLabel.setText(palkka.toString() + "e/h");
    }

    public void lataaTiedostosta(String tiedostonNimi) {
        try (Scanner tiedostonLukija = new Scanner(new File(tiedostonNimi))) {
            while (tiedostonLukija.hasNextLine()) {
                String rivi = tiedostonLukija.nextLine();
                String[] osat = rivi.split(",");
                DateTimeFormatter muotoilija = DateTimeFormatter.ofPattern("dd-MM-yyyy");
                LocalDate pvm = LocalDate.parse(osat[0], muotoilija);
                double tunnit = Double.parseDouble(osat[1]);
                int turnaukset = Integer.parseInt(osat[2]);
                double sisaanostot = Double.parseDouble(osat[3]);
                double voitot = Double.parseDouble(osat[4]);
                lisaa(new Pelipaiva(pvm, tunnit, turnaukset, sisaanostot, voitot));
            }
        } catch (Exception e) {
            throw new RuntimeException("Lataus tiedostosta ei onnistunut");
        }
    }

    public void tallennaTiedostoon(String tiedostonNimi) {
        try {
            PrintWriter tiedostonKirjoittaja = new PrintWriter(tiedostonNimi);
            for (Pelipaiva pelipaiva : this.pelitilasto) {
                String rivi = pelipaiva.toString();
                tiedostonKirjoittaja.println(rivi);
            }

            tiedostonKirjoittaja.close();
        } catch (Exception e) {
            throw new RuntimeException("Virhe tiedostoon tallennettaessa");
        }
    }

    public double laskeRoiAjalta(Month kuukausi, int vuosi) {
        double tulokset = 0;
        double sisaanosto = 0;
        for (Pelipaiva pelipaiva : this.pelitilasto) {
            if (pelipaiva.getPvm().getYear() == vuosi && pelipaiva.getPvm().getMonth().equals(kuukausi)) {
                tulokset += pelipaiva.getTulos();
                sisaanosto += pelipaiva.getSisaanostot();
            }
        }
        if (sisaanosto == 0) {
            return 0;
        }
        return Math.round((tulokset / sisaanosto) * 10000.0) / 100.0;
    }

    public double laskePalkkaAjalta(Month kuukausi, int vuosi) {
        double tunnit = 0;
        double tulokset = 0;
        for (Pelipaiva pelipaiva : this.pelitilasto) {
            if (pelipaiva.getPvm().getYear() == vuosi && pelipaiva.getPvm().getMonth().equals(kuukausi)) {
                tulokset += pelipaiva.getTulos();
                tunnit += pelipaiva.getTunnit();
            }
        }
        if (tunnit == 0) {
            return 0;
        }
        return Math.round((tulokset / tunnit) * 100.0) / 100.0;
    }

    public double laskeTulosAjalta(Month kuukausi, int vuosi) {
        double tulokset = 0;
        for (Pelipaiva pelipaiva : this.pelitilasto) {
            if (pelipaiva.getPvm().getYear() == vuosi && pelipaiva.getPvm().getMonth().equals(kuukausi)) {
                tulokset += pelipaiva.getTulos();
            }
        }
        return tulokset;
    }

    public double laskeTunnitAjalta(Month kuukausi, int vuosi) {
        double tunnit = 0;
        for (Pelipaiva pelipaiva : this.pelitilasto) {
            if (pelipaiva.getPvm().getYear() == vuosi && pelipaiva.getPvm().getMonth().equals(kuukausi)) {
                tunnit += pelipaiva.getTunnit();
            }
        }

        return tunnit;
    }

    public LineChart luoKokonaistulosViivakaavio() {
        CategoryAxis aikaAkseli = new CategoryAxis();
        NumberAxis tulosAkseli = new NumberAxis();

        aikaAkseli.setLabel("Päivämäärä");
        tulosAkseli.setLabel("Voitto");
        LineChart<String, Number> kaavio = new LineChart<>(aikaAkseli, tulosAkseli);
        
        kaavio.setTitle("Kokonaistulos");

        XYChart.Series<String, Number> data = new XYChart.Series<>();

        DateTimeFormatter muotoilija = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        Collections.sort(this.pelitilasto, (p1, p2) -> p1.getPvm().compareTo(p2.getPvm()));

        double tulos = 0;

        for (Pelipaiva pelipaiva : this.pelitilasto) {
            tulos += pelipaiva.getTulos();
            data.getData().add(new XYChart.Data(muotoilija.format(pelipaiva.getPvm()), tulos));
        }

        kaavio.getData().add(data);
        kaavio.setCreateSymbols(false);
        kaavio.setLegendVisible(false);

        return kaavio;
    }

    public LineChart luoKuukausitulosViivakaavio(Month kuukausi, int vuosi) {
        CategoryAxis aikaAkseli = new CategoryAxis();
        NumberAxis tulosAkseli = new NumberAxis();
        
        Kaantaja kaantaja = new Kaantaja();
        
        aikaAkseli.setLabel("Päivä");
        tulosAkseli.setLabel("Voitto");
        LineChart<String, Number> kaavio = new LineChart<>(aikaAkseli, tulosAkseli);
        kaavio.setTitle(kaantaja.kaannaKuukausiSuomeksi(kuukausi) + ", " + vuosi);
        
        String[] paivat = new String[31];
        
        for(int i = 0; i <= 30; i++) {
            paivat[i] = String.valueOf(i+1);
        }
        
        aikaAkseli.setCategories(FXCollections.<String>observableArrayList(Arrays.asList(paivat)));

        XYChart.Series<String, Number> data = new XYChart.Series<>();

        DateTimeFormatter muotoilija = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        Collections.sort(this.pelitilasto, (p1, p2) -> p1.getPvm().compareTo(p2.getPvm()));

        double tulos = 0;

        for (Pelipaiva pelipaiva : this.pelitilasto) {
            if (pelipaiva.getPvm().getYear() == vuosi && pelipaiva.getPvm().getMonth().equals(kuukausi)) {
                tulos += pelipaiva.getTulos();
                data.getData().add(new XYChart.Data(paivat[pelipaiva.getPvm().getDayOfMonth()-1], tulos));
            }
        }

        kaavio.getData().add(data);
        kaavio.setCreateSymbols(false);
        kaavio.setLegendVisible(false);

        return kaavio;
    }

    public LineChart luoPalkkaViivakaavio() {
        CategoryAxis aikaAkseli = new CategoryAxis();
        NumberAxis palkkaAkseli = new NumberAxis();

        aikaAkseli.setLabel("Päivämäärä");
        palkkaAkseli.setLabel("Palkka");
        LineChart<String, Number> kaavio = new LineChart<>(aikaAkseli, palkkaAkseli);

        XYChart.Series<String, Number> data = new XYChart.Series<>();

        DateTimeFormatter muotoilija = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        Collections.sort(this.pelitilasto, (p1, p2) -> p1.getPvm().compareTo(p2.getPvm()));

        double tulos = 0;
        int tunnit = 0;

        for (Pelipaiva pelipaiva : this.pelitilasto) {
            tulos += pelipaiva.getTulos();
            tunnit += pelipaiva.getTunnit();
            data.getData().add(new XYChart.Data(muotoilija.format(pelipaiva.getPvm()), (tulos / tunnit)));
        }

        kaavio.getData().add(data);
        kaavio.setCreateSymbols(false);
        kaavio.setLegendVisible(false);

        return kaavio;
    }

    public LineChart luoRoiViivakaavio() {
        CategoryAxis aikaAkseli = new CategoryAxis();
        NumberAxis roiAkseli = new NumberAxis();

        aikaAkseli.setLabel("Päivämäärä");
        roiAkseli.setLabel("ROI %");
        LineChart<String, Number> kaavio = new LineChart<>(aikaAkseli, roiAkseli);

        XYChart.Series<String, Number> data = new XYChart.Series<>();

        DateTimeFormatter muotoilija = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        Collections.sort(this.pelitilasto, (p1, p2) -> p1.getPvm().compareTo(p2.getPvm()));

        double tulos = 0;
        double sisaanostot = 0;

        for (Pelipaiva pelipaiva : this.pelitilasto) {
            tulos += pelipaiva.getTulos();
            sisaanostot += pelipaiva.getSisaanostot();
            data.getData().add(new XYChart.Data(muotoilija.format(pelipaiva.getPvm()), ((tulos / sisaanostot) * 100)));
        }

        kaavio.getData().add(data);
        kaavio.setCreateSymbols(false);
        kaavio.setLegendVisible(false);

        return kaavio;
    }

    public BarChart luoTulosKuukausittainPylvaskaavio(int vuosi) {
        Kaantaja kaantaja = new Kaantaja();
        CategoryAxis xAkseli = new CategoryAxis();
        NumberAxis yAkseli = new NumberAxis();
        yAkseli.setLabel("Tulos");
        BarChart<String, Number> kaavio = new BarChart<>(xAkseli, yAkseli);
        kaavio.setLegendVisible(false);
        kaavio.setAnimated(false);
        kaavio.setVerticalGridLinesVisible(false);

        XYChart.Series tulokset = new XYChart.Series();
        for (int i = 1; i < 13; i++) {
            Month kuukausi = Month.of(i);
            tulokset.getData().add(new XYChart.Data(kaantaja.kaannaKuukausiSuomeksi(kuukausi), laskeTulosAjalta(kuukausi, vuosi)));
        }

        kaavio.getData().add(tulokset);
        kaavio.setTitle(String.valueOf(vuosi));

        return kaavio;
    }

    public BarChart luoPalkkaKuukausittainPylvaskaavio(int vuosi) {
        Kaantaja kaantaja = new Kaantaja();
        CategoryAxis xAkseli = new CategoryAxis();
        NumberAxis yAkseli = new NumberAxis();
        yAkseli.setLabel("Palkka");
        BarChart<String, Number> kaavio = new BarChart<>(xAkseli, yAkseli);
        kaavio.setLegendVisible(false);
        kaavio.setAnimated(false);
        kaavio.setVerticalGridLinesVisible(false);

        XYChart.Series tulokset = new XYChart.Series();
        for (int i = 1; i < 13; i++) {
            Month kuukausi = Month.of(i);
            tulokset.getData().add(new XYChart.Data(kaantaja.kaannaKuukausiSuomeksi(kuukausi), laskePalkkaAjalta(kuukausi, vuosi)));
        }

        kaavio.getData().add(tulokset);
        kaavio.setTitle(String.valueOf(vuosi));

        return kaavio;
    }

    public BarChart luoRoiKuukausittainPylvaskaavio(int vuosi) {
        Kaantaja kaantaja = new Kaantaja();
        CategoryAxis xAkseli = new CategoryAxis();
        NumberAxis yAkseli = new NumberAxis();
        yAkseli.setLabel("ROI");
        BarChart<String, Number> kaavio = new BarChart<>(xAkseli, yAkseli);
        kaavio.setLegendVisible(false);
        kaavio.setAnimated(false);
        kaavio.setVerticalGridLinesVisible(false);

        XYChart.Series tulokset = new XYChart.Series();
        for (int i = 1; i < 13; i++) {
            Month kuukausi = Month.of(i);
            tulokset.getData().add(new XYChart.Data(kaantaja.kaannaKuukausiSuomeksi(kuukausi), laskeRoiAjalta(kuukausi, vuosi)));
        }

        kaavio.getData().add(tulokset);
        kaavio.setTitle(String.valueOf(vuosi));

        return kaavio;
    }
    
//    public GridPane luoEnnuste() {
//        GridPane asettelu = new GridPane();
//        
//        asettelu.setPadding(new Insets(20, 20, 20, 20));
//        asettelu.setVgap(50);
//        asettelu.setHgap(100);
//        asettelu.setAlignment(Pos.CENTER);
//        
//        Label info = new Label("Palkan 95%:n luottamusväli");
//        info.setFont(Font.font("Times New Roman", 30));
//        
//        asettelu.add(info, 0, 0);
//        
//        double kokonaisKeskihajonta;
//        double kolmekymmentaKeskihajonta;
//        
//        double kokonaisYhteensa = 0;
//        double kolmekymmentaYhteensa;
//        
//        for (Pelipaiva pelipaiva : this.pelitilasto) {
//            double erotus = pelipaiva.getPalkka() - this.getPalkka();
//            kokonaisYhteensa += (erotus * erotus);
//        }
//        
//        kokonaisKeskihajonta = Math.sqrt(kokonaisYhteensa / (this.pelitilasto.size() - 1));
//        
//        asettelu.add(new Label(-1.96 * kokonaisKeskihajonta + this.getPalkka() + " - " + (1.96 * kokonaisKeskihajonta + this.getPalkka())), 1, 0);
//        
//        return asettelu;
//    }

}
