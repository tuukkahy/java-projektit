/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pokeritilasto2.logiikka;

import java.time.Month;

/**
 *
 * @author Dell
 */
public class Kaantaja {

    public String kaannaKuukausiSuomeksi(Month kuukausi) {
        String kaannos = "";
        
        switch (kuukausi) {
            case JANUARY:
                return "tammikuu";
            case FEBRUARY:
                return "helmikuu";
            case MARCH:
                return "maaliskuu";
            case APRIL:
                return "huhtikuu";
            case MAY:
                return "toukokuu";
            case JUNE:
                return "kesäkuu";
            case JULY:
                return "heinäkuu";
            case AUGUST:
                return "elokuu";
            case SEPTEMBER:
                return "syyskuu";
            case OCTOBER:
                return "lokakuu";
            case NOVEMBER:
                return "marraskuu";
            case DECEMBER:
                return "joulukuu";
            default:
                return "tammikuu";
        }
        
    }
    
        public Month kaannaKuukausiEnglanniksi(String kuukausi) {
        
        switch (kuukausi) {
            case "tammikuu":
                return Month.JANUARY;
            case "helmikuu":
                return Month.FEBRUARY;
            case "maaliskuu":
                return Month.MARCH;
            case "huhtikuu":
                return Month.APRIL;
            case "toukokuu":
                return Month.MAY;
            case "kesäkuu":
                return Month.JUNE;
            case "heinäkuu":
                return Month.JULY;
            case "elokuu":
                return Month.AUGUST;
            case "syyskuu":
                return Month.SEPTEMBER;
            case "lokakuu":
                return Month.OCTOBER;
            case "marraskuu":
                return Month.NOVEMBER;
            case "joulukuu":
                return Month.DECEMBER;
            default:
                return Month.JANUARY;
        }
        
    }
}
