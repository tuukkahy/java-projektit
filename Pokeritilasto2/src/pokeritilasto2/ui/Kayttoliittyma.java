/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pokeritilasto2.ui;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Optional;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import pokeritilasto2.domain.Pelipaiva;
import pokeritilasto2.logiikka.Pelitilasto;
import pokeritilasto2.logiikka.Kaantaja;

/**
 *
 * @author Dell
 */
public class Kayttoliittyma extends Application {
    
    private Pelitilasto pelit;
    private String tiedostonNimi;
    private boolean tallennettu;
    
    @Override
    public void init() throws Exception {
        this.pelit = new Pelitilasto();
        this.tiedostonNimi = "pelitilasto.txt";
        this.pelit.lataaTiedostosta(tiedostonNimi);
        this.tallennettu = true;
    }
    
    @Override
    public void start(Stage ikkuna) throws Exception {
        // Luodaan ylin asettelu, joka pysyy aina paikallaan
        BorderPane ikkunaAsettelu = new BorderPane();
        ikkunaAsettelu.setPrefSize(1200, 720);

        // Lisätään valikkorivi (siirrä metodiiin?)
        VBox ylarivi = luoValikkorivi();
        ikkunaAsettelu.setTop(ylarivi);

        // Lisätään pääasettelu keskelle.
        BorderPane paaAsettelu = new BorderPane();
        ikkunaAsettelu.setCenter(paaAsettelu);
        paaAsettelu.setPadding(new Insets(20, 20, 20, 20));

        // Lisätään ylänappien asettelu.
        HBox ylaNapit = new HBox();
        ylaNapit.setAlignment(Pos.CENTER);
        ylaNapit.setSpacing(20);
        ylaNapit.setPadding(new Insets(20, 20, 20, 20));
        paaAsettelu.setTop(ylaNapit);

        // Lisätään vasemmalle ROI ja palkka.
        VBox vasenTiedot = luoRoiJaPalkka();
        paaAsettelu.setLeft(vasenTiedot);

        // Luodaan lisäys-asettelu
        GridPane lisaysAsettelu = luoLisaysAsettelu();
        
        // Lisätään data-nappi
        Button dataNappi = new Button("Data");
        ylaNapit.getChildren().add(dataNappi);

        // Lisätään lisäys-nappi
        Button lisaaNappi = new Button("Lisää");
        ylaNapit.getChildren().add(lisaaNappi);

        // Lisätään tilastonappi.
        Button tilastoNappi = new Button("Tilastot");
        ylaNapit.getChildren().add(tilastoNappi);

        // Lisätään graafinappi.
        Button graafiNappi = new Button("Graafit");
        ylaNapit.getChildren().add(graafiNappi);
        
        
        dataNappi.setOnAction(event -> {
            paaAsettelu.setCenter(this.pelit.getPelitaulukko());
        });
        
        lisaaNappi.setOnAction(event -> {
            paaAsettelu.setCenter(lisaysAsettelu);
        });
        
        tilastoNappi.setOnAction(event -> {
            int vuosi = valitseVuosi();
            if (vuosi != -1) {
                Month kuukausi = valitseKuukausi();
                if (kuukausi != null) {
                    double roi = this.pelit.laskeRoiAjalta(kuukausi, vuosi);
                    double palkka = this.pelit.laskePalkkaAjalta(kuukausi, vuosi);
                    double tunnit = this.pelit.laskeTunnitAjalta(kuukausi, vuosi);
                    GridPane tilastoAsettelu = luoTilastoAsettelu(roi, palkka, tunnit, kuukausi, vuosi);
                    paaAsettelu.setCenter(tilastoAsettelu);
                }
            }
            
        });
        
        graafiNappi.setOnAction(event -> {
            String valinta = valitseGraafi();
            if (valinta.equals("Kokonaistulos")) {
                paaAsettelu.setCenter(this.pelit.luoKokonaistulosViivakaavio());
            }
            if (valinta.equals("Kuukausitulos")) {
                int vuosi = valitseVuosi();
                if (vuosi != -1) {
                    Month kuukausi = valitseKuukausi();
                    if (kuukausi != null) {
                        paaAsettelu.setCenter(this.pelit.luoKuukausitulosViivakaavio(kuukausi, vuosi));
                    }
                }
            }
            if (valinta.equals("Palkan kehitys")) {
                paaAsettelu.setCenter(this.pelit.luoPalkkaViivakaavio());
            }
            if (valinta.equals("ROI:n kehitys")) {
                paaAsettelu.setCenter(this.pelit.luoRoiViivakaavio());
            }
            if (valinta.equals("Tulos kuukausittain")) {
                int vuosi = valitseVuosi();
                if (vuosi != -1) {
                    paaAsettelu.setCenter(this.pelit.luoTulosKuukausittainPylvaskaavio(vuosi));
                }
            }
            if (valinta.equals("Palkka kuukausittain")) {
                int vuosi = valitseVuosi();
                if (vuosi != -1) {
                    paaAsettelu.setCenter(this.pelit.luoPalkkaKuukausittainPylvaskaavio(vuosi));
                }
            }
            if (valinta.equals("ROI kuukausittain")) {
                int vuosi = valitseVuosi();
                if (vuosi != -1) {
                    paaAsettelu.setCenter(this.pelit.luoRoiKuukausittainPylvaskaavio(vuosi));
                }
            }
        });
        

        //Asetetaan lopuksi näkymä ja näytetään ikkuna
        paaAsettelu.setCenter(this.pelit.getPelitaulukko());
        Scene nakyma = new Scene(ikkunaAsettelu);
        ikkuna.setScene(nakyma);
        ikkuna.setTitle("Pelitilasto");
        ikkuna.show();
        
        ikkuna.setOnCloseRequest(event -> {
            if (!(tallennettu)) {
                boolean peruuta = naytaVahvistus();
                if (!peruuta) {
                    event.consume();
                }
            }
        });
    }
    
    public VBox luoValikkorivi() {
        MenuBar valikkorivi = new MenuBar();
        VBox ylarivi = new VBox(valikkorivi);
        Menu tiedostoValikko = new Menu("Tiedosto");
        Menu muokkaaValikko = new Menu("Muokkaa");
        valikkorivi.getMenus().addAll(tiedostoValikko, muokkaaValikko);

        // Lisätään tallennusmahdollisuus tiedostovalikkoon.
        MenuItem tallennaValikkovalinta = new MenuItem("Tallenna");
        MenuItem lopetaValikkovalinta = new MenuItem("Lopeta");
        tiedostoValikko.getItems().addAll(tallennaValikkovalinta, lopetaValikkovalinta);

        // Lisätään rivin poistomahdollisuus muokkausvalikkoon.
        MenuItem poistaValikkovalinta = new MenuItem("Poista valittu rivi");
        muokkaaValikko.getItems().add(poistaValikkovalinta);
        
        tallennaValikkovalinta.setOnAction(event -> {
            this.pelit.tallennaTiedostoon(tiedostonNimi);
            this.tallennettu = true;
        });
        
        lopetaValikkovalinta.setOnAction(event -> {
            if (this.tallennettu) {
                    Platform.exit();
                }
                else {
                    naytaVahvistus();
                }
        });
        
        poistaValikkovalinta.setOnAction(event -> {
            Pelipaiva poistettava = (Pelipaiva) this.pelit.getPelitaulukko().getSelectionModel().getSelectedItem();
            this.pelit.poista(poistettava);
            this.tallennettu = false;
        });
        
        return ylarivi;
        
    }
    
    public GridPane luoLisaysAsettelu() {
        // Luodaan perusta asettelulle
        GridPane lisaysAsettelu = new GridPane();
        lisaysAsettelu.setPadding(new Insets(20, 20, 20, 20));
        lisaysAsettelu.setVgap(10);
        lisaysAsettelu.setHgap(10);
        lisaysAsettelu.setAlignment(Pos.CENTER);

        // Lisätään kaikki elementit asetteluun.
        lisaysAsettelu.add(new Label("pvm"), 0, 0);
        DatePicker pvmValitsin = new DatePicker(LocalDate.now());
        lisaysAsettelu.add(pvmValitsin, 1, 0);
        lisaysAsettelu.add(new Label("tunnit"), 0, 1);
        TextField tuntiKentta = new TextField();
        lisaysAsettelu.add(tuntiKentta, 1, 1);
        lisaysAsettelu.add(new Label("turnaukset"), 0, 2);
        TextField turnausKentta = new TextField();
        lisaysAsettelu.add(turnausKentta, 1, 2);
        lisaysAsettelu.add(new Label("sisäänostot"), 0, 3);
        TextField sisaanostoKentta = new TextField();
        lisaysAsettelu.add(sisaanostoKentta, 1, 3);
        lisaysAsettelu.add(new Label("voitot"), 0, 4);
        TextField voitotKentta = new TextField();
        lisaysAsettelu.add(voitotKentta, 1, 4);
        Button lisaaNappi = new Button("Lisää");
        lisaaNappi.setDefaultButton(true);
        lisaysAsettelu.add(lisaaNappi, 0, 5);
        Button tyhjennaNappi = new Button("Tyhjennä");
        tyhjennaNappi.setCancelButton(true);
        lisaysAsettelu.add(tyhjennaNappi, 1, 5);

        // Lisää napin toiminnallisuus, nappi painettaessa tarkastetaan arvot
        lisaaNappi.setOnAction(event -> {
            try {
                double tunnit = Double.parseDouble(tuntiKentta.getText().trim());
                int turnaukset = Integer.parseInt(turnausKentta.getText().trim());
                double sisaanostot = Double.parseDouble(sisaanostoKentta.getText().trim());
                double voitot = Double.parseDouble(voitotKentta.getText().trim());
                LocalDate pvm = pvmValitsin.getValue();
                
                this.pelit.lisaa(new Pelipaiva(pvm, tunnit, turnaukset, sisaanostot, voitot));
                naytaIlmoitus("Pelipäivä lisätty onnistuneesti");
                tyhjennaNappi.fire();
                this.tallennettu = false;
                
            } catch (NumberFormatException e) {
                naytaVirhe("Virheelliset arvot!");
            }
        });
        
        tyhjennaNappi.setOnAction(event -> {
            tuntiKentta.clear();
            turnausKentta.clear();
            sisaanostoKentta.clear();
            voitotKentta.clear();
        });
        
        return lisaysAsettelu;
    }
    
    public VBox luoRoiJaPalkka() {
        VBox vasenTiedot = new VBox();
        vasenTiedot.setAlignment(Pos.CENTER);
        vasenTiedot.setSpacing(10);
        vasenTiedot.setPadding(new Insets(20, 20, 20, 20));
        vasenTiedot.getChildren().add(new Label("ROI"));
        Label roiTeksti = this.pelit.getRoiLabel();
        vasenTiedot.getChildren().add(roiTeksti);
        vasenTiedot.getChildren().add(new Label("---"));
        vasenTiedot.getChildren().add(new Label("Palkka"));
        Label palkkaTeksti = this.pelit.getPalkkaLabel();
        vasenTiedot.getChildren().add(palkkaTeksti);
        
        return vasenTiedot;
        
    }
    
    private static void naytaVirhe(String viesti) {
        Alert virhe = new Alert(AlertType.ERROR);
        virhe.setTitle("Virhe");
        virhe.setHeaderText(null);
        virhe.setContentText(viesti);
        
        virhe.showAndWait();
    }
    
    private static void naytaIlmoitus(String viesti) {
        Alert ilmoitus = new Alert(AlertType.INFORMATION);
        ilmoitus.setTitle("Info");
        ilmoitus.setHeaderText(null);
        ilmoitus.setContentText(viesti);
        
        ilmoitus.showAndWait();
    }
    
    private boolean naytaVahvistus() {
        Alert vahvistus = new Alert(AlertType.CONFIRMATION);
        vahvistus.setTitle("Tallennetaanko?");
        vahvistus.setHeaderText("Haluatko tallentaa tiedot?");
        vahvistus.setContentText("Valitse");
        
        ButtonType kyllaNappi = new ButtonType("Kyllä");
        ButtonType eiNappi = new ButtonType("Ei");
        ButtonType peruutaNappi = new ButtonType("Peruuta", ButtonBar.ButtonData.CANCEL_CLOSE);
        
        vahvistus.getButtonTypes().setAll(kyllaNappi, eiNappi, peruutaNappi);
        
        Optional<ButtonType> tulos = vahvistus.showAndWait();
        if (tulos.get() == kyllaNappi) {
            this.pelit.tallennaTiedostoon(this.tiedostonNimi);
            Platform.exit();
            return true;
        }
        else if (tulos.get() == eiNappi) {
            Platform.exit();
            return true;
        }
        else {
            vahvistus.close();
            return false;
        }
    }
    
    public int valitseVuosi() {
        ArrayList<Integer> valinnat = new ArrayList<>();
        for (Pelipaiva pelipaiva : pelit.getPelitilasto()) {
            if (!valinnat.contains(pelipaiva.getPvm().getYear())) {
                valinnat.add(pelipaiva.getPvm().getYear());
            }
        }
        
        ChoiceDialog<Integer> dialogi = new ChoiceDialog<>(LocalDate.now().getYear(), valinnat);
        dialogi.setTitle("Valitse vuosi");
        dialogi.setHeaderText(null);
        dialogi.setContentText("Valitse vuosi");
        
        Optional<Integer> tulos = dialogi.showAndWait();
        if (tulos.isPresent()) {
            return tulos.get();
        }
        
        return -1;
    }
    
    public Month valitseKuukausi() {
        ArrayList<String> valinnat = new ArrayList<>();
        Kaantaja kaantaja = new Kaantaja();
        valinnat.add("tammikuu");
        valinnat.add("helmikuu");
        valinnat.add("maaliskuu");
        valinnat.add("huhtikuu");
        valinnat.add("toukokuu");
        valinnat.add("kesäkuu");
        valinnat.add("heinäkuu");
        valinnat.add("elokuu");
        valinnat.add("syyskuu");
        valinnat.add("lokakuu");
        valinnat.add("marraskuu");
        valinnat.add("joulukuu");
        
        ChoiceDialog<String> dialogi = new ChoiceDialog<>("tammikuu", valinnat);
        dialogi.setTitle("Valitse kuukausi");
        dialogi.setHeaderText(null);
        dialogi.setContentText("Valitse kuukausi");
        
        Optional<String> tulos = dialogi.showAndWait();
        if (tulos.isPresent()) {
            Month kuukausi = kaantaja.kaannaKuukausiEnglanniksi(tulos.get());
            return kuukausi;
        }
        
        return null;
    }
    
    /*
    Seuraava metodi pitäisi siirtää Pelitilasto-luokkaan.
    */
    
    public GridPane luoTilastoAsettelu(double roi, double palkka, double tunnit, Month kuukausi, int vuosi) {
        GridPane asettelu = new GridPane();
        
        Kaantaja kaantaja = new Kaantaja();
        
        asettelu.setPadding(new Insets(20, 20, 20, 20));
        asettelu.setVgap(50);
        asettelu.setHgap(100);
        asettelu.setAlignment(Pos.CENTER);
        
        Label roiTeksti = new Label(String.valueOf(roi) + " %");
        roiTeksti.setFont(Font.font("Times New Roman", 30));
        Label palkkaTeksti = new Label(String.valueOf(palkka) + " e/t");
        palkkaTeksti.setFont(Font.font("Times New Roman", 30));
        Label tunnitTeksti = new Label(String.valueOf(tunnit) + " t");
        tunnitTeksti.setFont(Font.font("Times New Roman", 30));
        if (roi < 0) {
            roiTeksti.setTextFill(Color.RED);
        } else if (roi > 0 && this.pelit.getRoi() > roi) {
            roiTeksti.setTextFill(Color.ORANGE);
        } else if (this.pelit.getRoi() < roi) {
            roiTeksti.setTextFill(Color.GREEN);
        } else {
            roiTeksti.setTextFill(Color.YELLOW);
        }
        
        if (palkka < 3.0) {
            palkkaTeksti.setTextFill(Color.RED);
        } else if (palkka >= 3.0 && this.pelit.getPalkka() > palkka) {
            palkkaTeksti.setTextFill(Color.ORANGE);
        } else if (this.pelit.getPalkka() < palkka) {
            palkkaTeksti.setTextFill(Color.GREEN);
        } else {
            palkkaTeksti.setTextFill(Color.YELLOW);
        }
        
        Label vuosiJaKuukausi = new Label(kaantaja.kaannaKuukausiSuomeksi(kuukausi) + ", " + String.valueOf(vuosi));
        vuosiJaKuukausi.setFont(Font.font("Times New Roman", FontPosture.ITALIC, 30));
        asettelu.add(vuosiJaKuukausi, 0, 0);
        GridPane.setColumnSpan(vuosiJaKuukausi, 2);
        
        Label roiEsite = new Label("ROI:");
        roiEsite.setFont(Font.font("Times New Roman", FontWeight.BOLD, 30));
        asettelu.add(roiEsite, 0, 1);
        Label palkkaEsite = new Label("Palkka:");
        palkkaEsite.setFont(Font.font("Times New Roman", FontWeight.BOLD, 30));
        asettelu.add(palkkaEsite, 0, 2);
        Label tuntiEsite = new Label("Tunnit:");
        tuntiEsite.setFont(Font.font("Times New Roman", FontWeight.BOLD, 30));
        asettelu.add(tuntiEsite, 0, 3);
        
        asettelu.add(roiTeksti, 1, 1);
        asettelu.add(palkkaTeksti, 1, 2);
        asettelu.add(tunnitTeksti, 1, 3);
        
        return asettelu;
    }
    
    public String valitseGraafi() {
        ArrayList<String> valinnat = new ArrayList<>();
        valinnat.add("Kokonaistulos");
        valinnat.add("Kuukausitulos");
        valinnat.add("Palkan kehitys");
        valinnat.add("ROI:n kehitys");
        valinnat.add("Tulos kuukausittain");
        valinnat.add("Palkka kuukausittain");
        valinnat.add("ROI kuukausittain");
        
        ChoiceDialog<String> dialogi = new ChoiceDialog<>("Kokonaistulos", valinnat);
        dialogi.setTitle("Valitse graafi");
        dialogi.setHeaderText(null);
        dialogi.setContentText("Valitse graafi");
        
        Optional<String> tulos = dialogi.showAndWait();
        if (tulos.isPresent()) {
            return tulos.get();
        }
        
        return "nada";
    }
    
}
