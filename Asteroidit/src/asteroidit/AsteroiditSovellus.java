/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asteroidit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 *
 * @author Käyttäjä
 */
public class AsteroiditSovellus extends Application {

    public static final int KORKEUS = 400;
    public static final int LEVEYS = 600;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage ikkuna) throws Exception {
        Pane ruutu = new Pane();
        ruutu.setPrefSize(LEVEYS, KORKEUS);
        
        // Teksti pisteiden laskua varten
        AtomicInteger pisteet = new AtomicInteger();
        Text pisteetTeksti = new Text(10, 20, "Pisteet: 0");
        ruutu.getChildren().add(pisteetTeksti);

        Alus alus = new Alus(LEVEYS / 2, KORKEUS / 2);
        ruutu.getChildren().add(alus.getHahmo());

        List<Asteroidi> asteroidit = new ArrayList<>();
        for (int i = 4; i < 9; i++) {
            Random arpoja = new Random();
            Asteroidi asteroidi = new Asteroidi(arpoja.nextInt(LEVEYS), arpoja.nextInt(KORKEUS));
            asteroidit.add(asteroidi);
        }
        asteroidit.forEach(asteroidi -> ruutu.getChildren().add(asteroidi.getHahmo()));

        List<Ammus> ammukset = new ArrayList<>();

        Scene nakyma = new Scene(ruutu);
        ikkuna.setScene(nakyma);
        ikkuna.setTitle("Asteroidit");
        ikkuna.show();

        // Kuuntelija
        Map<KeyCode, Boolean> painetutNapit = new HashMap<>();

        nakyma.setOnKeyPressed(event -> {
            painetutNapit.put(event.getCode(), true);
        });

        nakyma.setOnKeyReleased(event -> {
            painetutNapit.put(event.getCode(), false);
        });

        new AnimationTimer() {
            @Override
            public void handle(long now) {
                // Aluksen liikkuminen
                if (painetutNapit.getOrDefault(KeyCode.LEFT, false) || painetutNapit.getOrDefault(KeyCode.A, false)) {
                    alus.kaannaVasemmalle();
                }
                if (painetutNapit.getOrDefault(KeyCode.RIGHT, false) || painetutNapit.getOrDefault(KeyCode.D, false)) {
                    alus.kaannaOikealle();
                }
                if (painetutNapit.getOrDefault(KeyCode.UP, false) || painetutNapit.getOrDefault(KeyCode.W, false)) {
                    alus.kiihdyta();
                }

                // Ampuminen välilyönnillä
                if (painetutNapit.getOrDefault(KeyCode.SPACE, false) && ammukset.size() < 3) {
                    Ammus ammus = new Ammus((int) alus.getHahmo().getTranslateX(), (int) alus.getHahmo().getTranslateY());
                    ammus.getHahmo().setRotate(alus.getHahmo().getRotate());
                    ammukset.add(ammus);

                    ammus.kiihdyta();
                    ammus.setLiike(ammus.getLiike().normalize().multiply(3));
                    ruutu.getChildren().add(ammus.getHahmo());

                }

                // Kaikkien elementtien liikkuminen
                alus.liiku();
                asteroidit.forEach(asteroidi -> asteroidi.liiku());
                ammukset.forEach(ammus -> ammus.liiku());

                // Asteroidien törmäys aluksen kanssa.
                asteroidit.forEach(asteroidi -> {
                    if (alus.tormaa(asteroidi)) {
                        stop();
                    }
                });


                
                // Tutkitaan ensin ammusten ja asteroidien törmäykset
                ammukset.forEach(ammus -> {
                    asteroidit.forEach(asteroidi -> {
                        if (ammus.tormaa(asteroidi)) {
                            ammus.setElossa(false);
                            asteroidi.setElossa(false);
                        }
                    });
                    
                    if (!ammus.isElossa()) {
                        pisteetTeksti.setText("Pisteet: " + pisteet.addAndGet(1000));
                    }
                });
                
                //Poistetaan törmänneet ammukset sekä ammuslistalta että ruudulta
                ammukset.stream()
                        .filter(ammus -> !ammus.isElossa())
                        .forEach(ammus -> ruutu.getChildren().remove(ammus.getHahmo()));
                ammukset.removeAll(ammukset.stream()
                                        .filter(ammus -> !ammus.isElossa())
                                        .collect(Collectors.toList()));
                
                // Poistetaan lopuksi törmänneet asteroidit
                asteroidit.stream()
                        .filter(asteroidi -> !asteroidi.isElossa())
                        .forEach(asteroidi -> ruutu.getChildren().remove(asteroidi.getHahmo()));
                asteroidit.removeAll(asteroidit.stream()
                                        .filter(asteroidi -> !asteroidi.isElossa())
                                        .collect(Collectors.toList()));
                
                
                // Lisätään vielä lopuksi uusien asteroidien luominen
                if (Math.random() < 0.01) {
                    Asteroidi asteroidi = new Asteroidi(LEVEYS/3, KORKEUS/3);
                    if (!asteroidi.tormaa(alus)) {
                        asteroidit.add(asteroidi);
                        ruutu.getChildren().add(asteroidi.getHahmo());
                    }
                }
            }

        }.start();

    }

}
