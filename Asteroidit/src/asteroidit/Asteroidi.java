/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asteroidit;

import java.util.Random;
import javafx.scene.shape.Polygon;

/**
 *
 * @author Käyttäjä
 */
public class Asteroidi extends Hahmo {
    
    private double pyorimisliike;

    public Asteroidi(int x, int y) {
        super(new MonikulmioTehdas().luoMonikulmio(), x, y);
        
        Random arpoja = new Random();
        
        super.getHahmo().setRotate(arpoja.nextInt(360));
        
        int kiihdytystenMaara = 1 + arpoja.nextInt(10);
        for (int i = 0; i < kiihdytystenMaara; i++) {
            super.kiihdyta();
        }
        
        this.pyorimisliike = 0.5 - arpoja.nextDouble();
    }

    @Override
    public void liiku() {
        super.liiku();
        super.getHahmo().setRotate(super.getHahmo().getRotate() + pyorimisliike);
    }
    
    
    
}
