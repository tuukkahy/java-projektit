/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asteroidit;

import javafx.geometry.Point2D;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Shape;


/**
 *
 * @author Käyttäjä
 */
public abstract class Hahmo {
    private Polygon hahmo;
    private Point2D liike;
    private boolean elossa;
    
    public Hahmo(Polygon monikulmio, int x, int y) {
        this.hahmo = monikulmio;
        this.hahmo.setTranslateX(x);
        this.hahmo.setTranslateY(y);
        
        this.liike = new Point2D(0,0);
        
        this.elossa = true;
    }

    public Polygon getHahmo() {
        return hahmo;
    }

    public Point2D getLiike() {
        return liike;
    }

    public void setLiike(Point2D liike) {
        this.liike = liike;
    }

    public boolean isElossa() {
        return elossa;
    }

    public void setElossa(boolean elossa) {
        this.elossa = elossa;
    }
    
    
    
    
    
        public void kaannaVasemmalle() {
        this.hahmo.setRotate(this.hahmo.getRotate() - 5);
    }
    
    public void kaannaOikealle() {
        this.hahmo.setRotate(this.hahmo.getRotate() + 5);
    }
    
    public void liiku() {
            this.hahmo.setTranslateX(this.hahmo.getTranslateX() + this.liike.getX());
            this.hahmo.setTranslateY(this.hahmo.getTranslateY() + this.liike.getY());
            
            if (this.hahmo.getTranslateX() < 0) {
                this.hahmo.setTranslateX(AsteroiditSovellus.LEVEYS);
            }
            if (this.hahmo.getTranslateY() < 0) {
                this.hahmo.setTranslateY(AsteroiditSovellus.KORKEUS);
            }
            if (this.hahmo.getTranslateX() > AsteroiditSovellus.LEVEYS) {
                this.hahmo.setTranslateX(0);
            }
            if (this.hahmo.getTranslateY() > AsteroiditSovellus.KORKEUS) {
                this.hahmo.setTranslateY(0);
            }
    }
    
    public void kiihdyta() {
        double muutosX = Math.cos(Math.toRadians(this.hahmo.getRotate()));
        double muutosY = Math.sin(Math.toRadians(this.hahmo.getRotate()));
        
        this.liike = this.liike.add(0.05 * muutosX,  0.05 * muutosY);
    }
    
    public boolean tormaa(Hahmo toinen) {
        Shape tormaysalue = Shape.intersect(this.hahmo, toinen.getHahmo());
        return tormaysalue.getBoundsInLocal().getWidth() != -1;
    }
    
}
