/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taulukkotesti;

/**
 *
 * @author Dell
 */
public class Peli {
    private double tunnit;
    private int turnaukset;
    private double voitot;

    public Peli(double tunnit, int turnaukset, double voitot) {
        this.tunnit = tunnit;
        this.turnaukset = turnaukset;
        this.voitot = voitot;
    }

    public double getTunnit() {
        return tunnit;
    }

    public int getTurnaukset() {
        return turnaukset;
    }

    public double getVoitot() {
        return voitot;
    }

    public double getKeskimVoitto() {
        return voitot / turnaukset;
    }
    
    
    
}
