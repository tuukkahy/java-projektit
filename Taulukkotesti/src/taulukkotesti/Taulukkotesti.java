/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taulukkotesti;

import java.util.ArrayList;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

/**
 *
 * @author Dell
 */
public class Taulukkotesti extends Application {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(Taulukkotesti.class);
    }

    @Override
    public void start(Stage ikkuna) throws Exception {
        BorderPane asettelu = new BorderPane();
        asettelu.setPadding(new Insets(20, 20, 20, 20));
        asettelu.setTop(new Label("Taulukko"));
        asettelu.setPrefSize(800, 600);
        
        Button poista = new Button("Poista rivi");
        asettelu.setLeft(poista);
        

        
        TableView<Peli> taulukko = new TableView<Peli>();
        
        TableColumn tunnitSarake = new TableColumn("Tunnit");
        tunnitSarake.setCellValueFactory(new PropertyValueFactory<Peli, Double>("tunnit"));
        
        TableColumn turnausSarake = new TableColumn("Turnaukset");
        turnausSarake.setCellValueFactory(new PropertyValueFactory<Peli, Integer>("turnaukset"));
        
        TableColumn voitotSarake = new TableColumn("Voitot");
        voitotSarake.setCellValueFactory(new PropertyValueFactory<Peli, Double>("voitot"));
        
        TableColumn keskimVoittoSarake = new TableColumn("Keskim. voitto");
        keskimVoittoSarake.setCellValueFactory(new PropertyValueFactory<Peli, Double>("keskimVoitto"));
        
        taulukko.getColumns().addAll(tunnitSarake, turnausSarake, voitotSarake, keskimVoittoSarake);
        
        Peli peli1 = new Peli(6.5, 12, 200);
        Peli peli2 = new Peli(4, 6, -120);
        Peli peli3 = new Peli(8, 20, 340);
        
        ArrayList<Peli> pelit = new ArrayList<>();
        pelit.add(peli1);
        pelit.add(peli2);
        pelit.add(peli3);
        
        taulukko.getItems().add(peli1);
        taulukko.getItems().add(peli2);
        taulukko.getItems().add(peli3);
        
        
        poista.setOnAction(event -> {
            Peli valittuRivi = taulukko.getSelectionModel().getSelectedItem();
            taulukko.getItems().remove(valittuRivi);
            pelit.stream()
                    .forEach(peli -> System.out.println(peli));
        });
        
        asettelu.setCenter(taulukko);
        
        Scene nakyma = new Scene(asettelu);
        ikkuna.setScene(nakyma);
        ikkuna.show();
    }
    
    
    
}
